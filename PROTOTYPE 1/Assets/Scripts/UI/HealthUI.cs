﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HealthUI : MonoBehaviour {

    // Use this for initialization
    public Text HealthText;
    public Health PlayerHealth;
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        HealthText.text = ("Health:" + PlayerHealth.CurrentHp);
	}
}
