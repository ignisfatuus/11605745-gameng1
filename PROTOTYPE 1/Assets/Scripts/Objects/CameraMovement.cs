﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

    // Use this for initialization
    public GameObject TargetGameObject;
    public float YOffset;
    public float z;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        FollowTarget();
	}

    void FollowTarget()
    {
        transform.position = new Vector3(gameObject.transform.position.x, TargetGameObject.transform.position.y + YOffset,z-5);
    }
}
