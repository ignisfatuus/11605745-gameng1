﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public float VerticalSpeed;
    public float HorizontalSpeed;
    float directionX;
    Rigidbody2D rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();   
    }
    // Update is called once per frame
    void Update()
    {
        Move();
    }

    void Move()
    {
        directionX = Input.acceleration.x * HorizontalSpeed;

        float movingDirection;
        movingDirection = Input.GetAxis("Horizontal");
        movingDirection *= HorizontalSpeed * Time.deltaTime;


        transform.position = new Vector2(Mathf.Clamp(transform.position.x, -2.5f, 2.5f), transform.position.y);
        transform.position += new Vector3(movingDirection, VerticalSpeed);
    }

    private void FixedUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            directionX = -1 * HorizontalSpeed*7;
        }

        if (Input.GetMouseButtonDown(1))
        {
            directionX = 1 * HorizontalSpeed*7;
        }
        rb.velocity = new Vector2(directionX, 0f);
    }
}
