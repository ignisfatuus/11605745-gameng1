﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour {

    public GameObject ObjectToSpawn;
    public float SpawnInterval = 1.0f;
    public int SpawnCount = 1;
    public int MaxSpawnCount;
    public float Radius = 5.0f;
    public GameObject Player;
    public Vector2 MinPosition, MaxPosition;
    private List<GameObject> spawned = new List<GameObject>();
    private List<GameObject> pooledObjects = new List<GameObject>();

    void Start()
    {
        StartCoroutine(SpawnTask());
    }

    IEnumerator SpawnTask()
    {
        for (int x = 0; x < MaxSpawnCount; x++)
        {
            {
                // Wait for spawn interval
                yield return new WaitForSeconds(SpawnInterval);

                for (int i = 0; i < SpawnCount; i++)
                {

                    // Spawn the object. If we have an object in the pool, use that instead. Else, instantiate.
                    GameObject obj;
                    if (pooledObjects.Count > 0)
                    {
                        obj = pooledObjects[0];
                        pooledObjects.RemoveAt(0);
                        obj.SetActive(true);
                    }
                    else
                    {
                        obj = Instantiate(ObjectToSpawn);
                    }

                    // Randomize position
                    SetPosition();
                    obj.transform.position = new Vector3(Random.Range(MinPosition.x, MaxPosition.x), Random.Range(MinPosition.y, MaxPosition.y), 0);

                    spawned.Add(obj);


                }

            }
        }
    }

    void SetPosition()
    {
        MinPosition.y = Player.transform.position.y - 5;
        MaxPosition.y = MinPosition.y - 5;
        MinPosition.x = -2;
        MaxPosition.x = 2;
    }
}
