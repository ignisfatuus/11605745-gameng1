﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterMovement : MonoBehaviour {

    // Use this for initialization
    public float FirstTargetPosition;
    public float SecondTargetPosition;
    public float MoveSpeed;
    public int ScoreValue;

    public Score ScoreScript;

    private bool hasReachedPointA;
	void Start ()
    {
        
        hasReachedPointA = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        Move();
	}

    void Move()
    {
        if (!hasReachedPointA)
        {
            gameObject.transform.position = Vector2.MoveTowards(transform.position, new Vector3(FirstTargetPosition, gameObject.transform.position.y), MoveSpeed * Time.deltaTime);
            if (gameObject.transform.position.x == FirstTargetPosition)
            {
                hasReachedPointA = true;
            }
        }
        else if (hasReachedPointA)
        {
            gameObject.transform.position = Vector2.MoveTowards(transform.position, new Vector3(SecondTargetPosition, gameObject.transform.position.y), MoveSpeed * Time.deltaTime);
            if (gameObject.transform.position.x == SecondTargetPosition)
            {
                hasReachedPointA = false;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<Health>().TakeDamage(100);
            ScoreScript.IncreaseScore(ScoreValue);
            Destroy(gameObject);
        }
    }
}
