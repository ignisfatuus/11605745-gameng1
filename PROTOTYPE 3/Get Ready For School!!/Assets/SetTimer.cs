﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetTimer : MonoBehaviour {

    public Text UiText;
	// Use this for initialization
	void Start () {
        UiText = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        UiText.text = TimeManager.Instance.TimeCount.ToString();
	}
}
