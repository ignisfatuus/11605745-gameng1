﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dirt : MonoBehaviour
{
    public int DirtNeededAmount;
    [SerializeField] private bool madeContact;
    public DirtChecker DirtCheckerObject;
    private SpriteRenderer ObjectSpriteRenderer;
    public Sprite[] SpriteList;
	// Use this for initialization
	void Start ()
    {
        ObjectSpriteRenderer = GetComponent<SpriteRenderer>();
        madeContact = false;
	}
	
	// Update is called once per frame
	void Update () {
        Animation();
	}

    public void ReduceDirt()
    {
        Debug.Log("Linis Linis");
        DirtNeededAmount -= 1;
        RemoveDirt();
    }

    public void DoneMakingContact()
    {
        if (!madeContact) return;
        Debug.Log("Linis Linis");
        madeContact = false;
    }

    void RemoveDirt()
    {
        if (DirtNeededAmount > 0) return;
        if (DirtNeededAmount <= 0) gameObject.SetActive(false);
        DirtCheckerObject.DirtNumber += 1;
        DirtCheckerObject.CheckIfAllClean();

    }

    void Animation()
    {
       
            ObjectSpriteRenderer.sprite = SpriteList[DirtNeededAmount];
        
    }

    
}
