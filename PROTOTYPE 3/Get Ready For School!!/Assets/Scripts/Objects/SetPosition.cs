﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetPosition : MonoBehaviour {

    public bool HasBeenSet=false;
    public PairSystem PairSystemClass;
    public ClothesChecker CheckerNgClothes;
    	// Use this for initialization
	void Start ()
    {
        PairSystemClass = gameObject.GetComponent<PairSystem>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.tag == "TargetSpot")
        {
  

            if (HasBeenSet) return;
            if (PairSystemClass.PairNumber != collision.GetComponent<PairSystem>().PairNumber) return;
            Vector3 NewPosition = new Vector3(collision.transform.position.x, collision.transform.position.y, gameObject.transform.position.z);
            gameObject.transform.position = NewPosition;
            HasBeenSet = true;
            gameObject.GetComponent<DragItems>().enabled = false;
            CheckerNgClothes.NumberOfSetClothes += 1;
            CheckerNgClothes.CheckClothesHasBeenSet();
        }
    }
}
