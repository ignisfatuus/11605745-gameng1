﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGSound : MonoBehaviour {

    private static BGSound instance;
    public static BGSound Instance { get { return instance; } }
    public AudioSource AudioSourceOfObject;
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this.gameObject);

        DontDestroyOnLoad(this.gameObject);
    
    }

    // Use this for initialization
    void Start ()
    {
        AudioSourceOfObject = GetComponent<AudioSource>();

    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void PlayNextSceneSound()
    {

        AudioSourceOfObject.Play();
    }
}
