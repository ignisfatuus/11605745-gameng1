﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class TimeManager : MonoBehaviour {
    [SerializeField] public float TimeCount;
    public float AverageTime;
    public int NumberOfTimers;
    public float MaxTimeCount;
    public float DecrementInSeconds;
    private static TimeManager instance;
    public static TimeManager Instance { get { return instance; } }

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this.gameObject);

        DontDestroyOnLoad(this.gameObject);


    }
    private void OnEnable()
    {
        TimeCount = MaxTimeCount;
        StartCoroutine(DecreaseTime(DecrementInSeconds));
    }
    public float GetTimeCount()
    {
        return TimeCount;
    }
   
    

    

    IEnumerator DecreaseTime(float decrement)
    {
        while (TimeCount > 0)
        {
            TimeCount -= decrement;
            yield return new WaitForSeconds(decrement);

        }

    }

    // Update is called once per frame
    void Update()
    {
        //
        Debug.Log(TimeCount);
        if (TimeCount == 0)
        {
            
            SceneManager.LoadScene(7);
            TimeManager.instance.gameObject.SetActive(false);
        }
    }

    public void ResetTimeCount()
    {
       
            TimeCount = MaxTimeCount;
        
    }

    public void AddAverageTime()
    {
        float CurrentTime = MaxTimeCount - TimeCount;
        NumberOfTimers += 1;
        AverageTime += CurrentTime;
        if (NumberOfTimers == 5)
        {
            AverageTime /= NumberOfTimers;
        }
    }
}
