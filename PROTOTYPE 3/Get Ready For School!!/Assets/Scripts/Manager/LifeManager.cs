﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeManager : MonoBehaviour {

    public int NumberOfLives=3;
    public bool isAlive { get { return NumberOfLives <= 0; } private set { } }
    public TimeManager TimeManagerClass;
    private static LifeManager instance;
    public static LifeManager Instance { get { return instance; } }

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this.gameObject);

        DontDestroyOnLoad(this.gameObject);

    }
    void Start()
    {
        TimeManagerClass = FindObjectOfType<TimeManager>();
    }

    // Update is called once per frame
    void Update()
    {
        decreaseLifeAmount(1);
    }

    void CapLife()
    {
        if (NumberOfLives <= 0)
        {
            NumberOfLives = 0;
        }
    }

    void decreaseLifeAmount(int decrement)
    {
        if (NumberOfLives == 0) return;
        if (TimeManagerClass.TimeCount == 0)
        {
            NumberOfLives -= decrement;
        }
    }

    void increaseLifeAmount(int increment)
    {
        NumberOfLives += increment;
    }
}
