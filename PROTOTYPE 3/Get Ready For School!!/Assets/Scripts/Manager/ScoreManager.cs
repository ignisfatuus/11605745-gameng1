﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour {

    // Use this for initialization
    [SerializeField] public int Score;

    private static ScoreManager instance;
    public static ScoreManager Instance { get { return instance; } }

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this.gameObject);

        DontDestroyOnLoad(this.gameObject);

    }

    void Start ()
    {
        Score = 0;
	}

    public void increaseScoreAmount(int increment)
    {
        Score += increment;
    }
}
