﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ManageScene : MonoBehaviour {

    // Use this for initialization

    public TimeManager TimeManagerClass;
    private static ManageScene instance;
    private bool hasEnded;
    public static ManageScene Instance { get { return instance; } }

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this.gameObject);

        DontDestroyOnLoad(this.gameObject);
   
    }
    private void Start()
    {
        hasEnded = false;
        TimeManagerClass = FindObjectOfType<TimeManager>();
    }
    // Update is called once per frame
    void Update ()
    {
        
      
   
      
        
	}

    public void MoveToNextScene()
    {
        BGSound.Instance.PlayNextSceneSound();
        TimeManager.Instance.AddAverageTime();
        int nextSceneIndex = SceneManager.GetActiveScene().buildIndex + 1;
       
       
         if (SceneManager.sceneCountInBuildSettings > nextSceneIndex)
        {
            if (SceneManager.sceneCountInBuildSettings == 5)
            {
                SceneManager.LoadScene(6);
                return;
            }
            SceneManager.LoadScene(nextSceneIndex);
            TimeManagerClass.ResetTimeCount();
        }
        else if (nextSceneIndex==SceneManager.sceneCountInBuildSettings)
        {
            nextSceneIndex = 0;
            SceneManager.LoadScene(0);
            TimeManagerClass.ResetTimeCount();
        }
    }

    public void RestartScene()
    {
        SceneManager.LoadScene(1);
        TimeManager.Instance.gameObject.SetActive(true);
        TimeManager.Instance.ResetTimeCount();
        
    }

    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }
}
