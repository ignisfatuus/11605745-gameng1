﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    // Use this for initialization
    private static GameManager instance;
    public static GameManager Instance { get { return instance; } }
    public GameObject[] Managers;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this.gameObject);

        DontDestroyOnLoad(this.gameObject);
        for (int x = 0; x < Managers.Length; x++)
        {
            Instantiate(Managers[x]);
        }
    }


    void Start ()
    {
        
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}
