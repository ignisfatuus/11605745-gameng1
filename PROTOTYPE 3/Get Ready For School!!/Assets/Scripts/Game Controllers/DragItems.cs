﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragItems : MonoBehaviour {

    public GameObject GameObjectToDrag;
    public bool IsHeld;
    public bool isSelected;
    Vector3 MousePosition;
    // Use this for initialization
    void Start () {
        IsHeld = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        checkIfSelected(MousePosition);
        checkIfHeld(MousePosition);
        TouchControls();
        MouseControls();

	}

    void TouchControls()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            switch (touch.phase)
            {
                case TouchPhase.Began:

                    Vector3 touchPosition = Camera.main.ScreenToWorldPoint(touch.position);
                    touchPosition.z = 0f;
                    GameObjectToDrag.transform.position = (touchPosition);
                    return;
            }
        }
    }

    void MouseControls()
    {

         MousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        MousePosition.z = 0;
        Collider ObjectsCollider = GetComponent<Collider>();
        if (!IsHeld) return;
        if (!isSelected) return;
        if (Input.GetMouseButton(0)) 
        {
            GameObjectToDrag.transform.position = new Vector3(MousePosition.x,MousePosition.y,GameObjectToDrag.transform.position.z);

        }
    }

    void checkIfHeld(Vector3 MousePosition)
    {
        Collider ObjectsCollider = GetComponent<Collider>();
        if (ObjectsCollider.bounds.Contains(MousePosition))
        {
            Debug.Log("Hiii");
            IsHeld = true;
        }
        else
        {
            IsHeld = false;
        }

    }

    void checkIfSelected(Vector3 MousePosition)
    {
        Collider ObjectsCollider = GetComponent<Collider>();
        if (Input.GetMouseButtonDown(0))
        {
            if (ObjectsCollider.bounds.Contains(MousePosition))
            {
                isSelected = true;
            }
            else
            {
                isSelected = false;
            }
        }
    }

}
