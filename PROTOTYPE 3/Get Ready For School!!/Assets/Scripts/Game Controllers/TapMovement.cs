﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapMovement : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        KeyboardControls();
	}

    void KeyboardControls()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x - 1, transform.position.y, transform.position.z);
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x + 1, transform.position.y, transform.position.z);
        }
    }
}
