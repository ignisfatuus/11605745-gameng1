﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WakeUp : MonoBehaviour {
    // Use this for initialization
    private ScoreManager ScoreManagerClass;
    private TimeManager TimeManagerClass;
    private ManageScene SceneManagerClass;
    public int amount;
    public int x;
    void Start()
    {
        ScoreManagerClass = FindObjectOfType<ScoreManager>();
        SceneManagerClass = FindObjectOfType<ManageScene>();
        TimeManagerClass = FindObjectOfType<TimeManager>();
    }

    // Update is called once per frame
    void Update()
    {
        ShakeControls();
    }

    private void ShakeControls()
    {

        if (x >= 5)
        {
            SceneManagerClass.MoveToNextScene();
            ScoreManagerClass.increaseScoreAmount(5);
            TimeManagerClass.ResetTimeCount();
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            x += 1;
        }
    }
}
