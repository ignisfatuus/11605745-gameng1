﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shake : MonoBehaviour {

    // Use this for initialization
   
    public int amount;
    public int x;
    Vector3 accelerationDir;
	void Start ()
    {
       
	}

    // Update is called once per frame
    void Update()
    {
        accelerationDir = Input.acceleration;
        ShakeControls();
    }

    private void ShakeControls()
    {

        if (x >= amount)
        {
            ManageScene.Instance.MoveToNextScene();
            ScoreManager.Instance.increaseScoreAmount(5);
            TimeManager.Instance.ResetTimeCount();
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            x += 1;
        }
        if (accelerationDir.sqrMagnitude>=5f)
        {
            x += 1;
        }
    }
}
