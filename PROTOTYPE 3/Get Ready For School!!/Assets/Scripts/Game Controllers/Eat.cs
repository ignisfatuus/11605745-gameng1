﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eat : MonoBehaviour {


    // Use this for initialization

    public int RequiredAmount=15;
    public int TapAmount;
    public SpriteRenderer Egg;
    public SpriteRenderer[] Bacon;
    public Sprite[] EggAnimations;
    public Sprite[] BaconAnimations;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ShakeControls();
    }

    private void ShakeControls()
    {

        if (TapAmount >= RequiredAmount)
        {
            ManageScene.Instance.MoveToNextScene();
            ScoreManager.Instance.increaseScoreAmount(5);
            TimeManager.Instance.ResetTimeCount();
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            TapAmount += 1;
            AnimateFood();
        }
    }

    public void AnimateFood()
    {
        Egg.sprite = EggAnimations[TapAmount];
        Bacon[0].sprite = BaconAnimations[TapAmount];
        Bacon[1].sprite = BaconAnimations[TapAmount];
        Bacon[2].sprite = BaconAnimations[TapAmount];
    }
}
