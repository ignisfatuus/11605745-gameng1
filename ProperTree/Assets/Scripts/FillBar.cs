﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class FillBar : MonoBehaviour
{
    public Image HealthBar; 
    public Health TargetHealthComponent;
    // Start is called before the first frame update
    void Start()
    {
        HealthBar = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        fillBarFromValue();
    }

    void fillBarFromValue()
    {
        HealthBar.fillAmount = TargetHealthComponent.CurrentHp / TargetHealthComponent.MaxHp;
    }


}
