﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{

    public Vector3 SpawnPosition;
    public GameObject Object;
    public float SpawnInterval;
    public int SpawnCount = 50;
    private List<GameObject> spawned = new List<GameObject>();
    private List<GameObject> pooledObjects = new List<GameObject>();
    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (Input.GetKeyDown(KeyCode.Space))
        {
            SpawnObject();
        }
	}

    void SpawnObject()
    {
        // Spawn the object. If we have an object in the pool, use that instead. Else, instantiate.
        GameObject obj;
        if (pooledObjects.Count > 0)
        {
            obj = pooledObjects[0];
            pooledObjects.RemoveAt(0);
            obj.SetActive(true);
        }
        else
        {
            obj = Instantiate(Object);
        }

        // Randomize position
        obj.transform.position = new Vector3(transform.position.x,transform.position.y);

        spawned.Add(obj);

        // Remove spawn after certain position
        
    }
}
