﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterControls : MonoBehaviour {

   
	public float VerticalInterval;
    private bool isUpEdged;
    private bool isDownEdged;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
        MovementInput();	
	}

	void MovementInput()
	{
      
        if (Input.GetKeyDown(KeyCode.W))
        {
            if (isUpEdged) return;
            transform.position += new Vector3(0, VerticalInterval);
            ConstrainControls();
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            if (isDownEdged) return;
            transform.position += new Vector3(0, -VerticalInterval);
            ConstrainControls();
        }
	}

    void ConstrainControls()
    {
        if (gameObject.transform.position.y >= VerticalInterval)
        {
            isUpEdged = true;
            isDownEdged = false; 
        }
        else if (gameObject.transform.position.y <= -VerticalInterval)
        {
            isDownEdged = true;
            isUpEdged = false;
        }
        else
        {
            isDownEdged = false;
            isUpEdged = false;
        }
   
    }


    
    

}
