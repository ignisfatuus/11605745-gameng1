﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {
    public float HorizontalVelocity;
    public float VerticalVelocity;
    // Use this for initialization
    void Start()
    {
        HorizontalVelocity *= Time.deltaTime;
        VerticalVelocity *= Time.deltaTime;
    }

    // Update is called once per frame
    void Update()
    {
        ApplyMovement();
    }

    void ApplyMovement()
    {
        transform.position += new Vector3(HorizontalVelocity, VerticalVelocity);
    }
   
}
