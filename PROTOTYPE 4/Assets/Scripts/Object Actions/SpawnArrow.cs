﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnArrow : MonoBehaviour {

    public GameObject Player;
    public ArrowMovement Arrow;
    public float Speed;
    public Inventory InventoryManager;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        SpawnArrowOnClick();	
	}

    void SpawnArrowOnClick()
    {
        if (InventoryManager.Arrows.Count <= 0) return;
        if (!Input.GetMouseButtonDown(0)) return;

        Vector2 direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - Player.transform.position;
        float angle = Mathf.Atan2(direction.x, direction.y) * Mathf.Rad2Deg;
        Debug.Log(angle);
        if (angle >= 0)
        {
            Arrow.gameObject.transform.localRotation = Quaternion.Euler(0, 0, 0);
            

        }
        if (angle < 0)
        {
            Arrow.gameObject.transform.localRotation = Quaternion.Euler(0, 180, 0);

        }
        Instantiate(Arrow.gameObject,new Vector3 (gameObject.transform.position.x,gameObject.transform.position.y),Arrow.gameObject.transform.rotation);

        Arrow.isFired = false;

        InventoryManager.Arrows.RemoveAt(0);

        
    }
   
}
