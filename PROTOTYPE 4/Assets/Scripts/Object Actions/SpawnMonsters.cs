﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnMonsters : MonoBehaviour {

    public float SpawnInterval;
    public int SpawnCount;
    public List<Nodes> SpawnNodes = new List<Nodes>();
    private List<GameObject> pooledObjects = new List<GameObject>();
    private List<GameObject> spawned = new List<GameObject>();

    public GameObject ObjectToSpawn;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(SpawnTask());
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator SpawnTask()
    {
        while (true)
        {

            yield return new WaitForSeconds(SpawnInterval);
            for (int i = 0; i < SpawnCount; i++)
            {
                GameObject obj;
                if (pooledObjects.Count > 0)
                {
                    obj = pooledObjects[0];
                    pooledObjects.RemoveAt(0);
                    obj.SetActive(true);
                }
                else
                {
                    obj = Instantiate(ObjectToSpawn);
                }
                // Set Position
                Transform SpawnNode;
                int number = Random.Range(0, SpawnNodes.Count);
                SpawnNode = SpawnNodes[number].transform;

                obj.transform.position = SpawnNode.transform.position;
                spawned.Add(obj);
            }

        }
    }

}
