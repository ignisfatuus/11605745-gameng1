﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : MonoBehaviour
{
    public GameObject Target;
    private Animator ObjectAnimator;
    public float Speed;
    public int MonsterNumber;
    public float Increment;
    public int ScoreAmount;
    public Score ScoreObject;
	// Use this for initialization
	void Start ()
    {
        ScoreObject = GameObject.Find("Rowel").GetComponent<Score>();
        Target = GameObject.Find("Rowel");
        Speed *= Time.deltaTime;
        StartCoroutine(IncreaseSpeed());
        ObjectAnimator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        MoveTowardsPlayer();
	}

    void MoveTowardsPlayer()
    {

        transform.position = Vector3.MoveTowards(transform.position, Target.transform.position, Speed);
        PlayAnimation();
    }

    void PlayAnimation()
    {
     
        Vector2 direction = Target.transform.position- transform.position;
        float angle = Mathf.Atan2(direction.x, direction.y) * Mathf.Rad2Deg;

        if (angle >= 0)
        {
            transform.localRotation = Quaternion.Euler(0, 180, 0);
            return;
        }
        if (angle < 0)
        {
            transform.localRotation = Quaternion.Euler(0, 0, 0);
            return;
        }
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<Health>().TakeDamage(20);
            
        }

        

    }

    public void AddScore()
    {
        ScoreObject.AddScore(ScoreAmount);
    }

    IEnumerator IncreaseSpeed()
    {
        while (true)
        {
            yield return new WaitForSeconds(3);
            float increment = Increment * Time.deltaTime;
            Speed += increment;
        }
    }

 
}
