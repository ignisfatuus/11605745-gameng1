﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowMovement : MonoBehaviour {

    // Use this for initialization
    public float Speed;
    public bool isFired;
    public bool isMovable;
    public bool MetPoint;
    public bool canDamage;
    public Crosshair CrosshairObject;

    void Start ()
    {

        isFired = false;
        StartCoroutine(DestroyInSeconds());
      
    }
	
	// Update is called once per frame
	void Update ()
    {
       FollowMouseCursor();

    }

    public void FollowMouseCursor()
    {

       
        if (!isMovable) return;
        if (isFired) return;
        Vector3 MousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
 
        gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, new Vector3(MousePosition.x,MousePosition.y), Speed);

        if (gameObject.transform.position.x == MousePosition.x)
        {
            MetPoint = true;
            isMovable = false;
            canDamage = false;
             
        }
     

    }

    void StopMovement()
    {

    }
 
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Monster")
        {
            if (!canDamage) return;
            other.gameObject.GetComponent<Health>().TakeDamage(20);
            other.gameObject.GetComponent<Monster>().AddScore();
            Destroy(gameObject);
        }
    }

    public IEnumerator MoveForSeconds()
    {

        yield return new WaitForSeconds(5);
        isFired = true;

    }

    IEnumerator DestroyInSeconds()
    {
        yield return new WaitForSeconds(1);
        Destroy(gameObject);
    }
}
