﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nodes : MonoBehaviour {

    public int WeaponCapacity;
    public bool isCapable {  get ; private set; }
    private List<GameObject> Weapons = new List<GameObject>();
	// Use this for initialization
	void Start ()
    {
        isCapable = true;
	}
	
	// Update is called once per frame
	void Update ()
    {
        CheckIfEmpty();
	}

   void CheckIfEmpty()
    {
        if (Weapons.Count>=WeaponCapacity)
        {
            isCapable = false;
        }
       if (Weapons.Count < WeaponCapacity)
        {
            isCapable = true;
        }
       
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Weapon")
        {
            if (other.bounds.Contains(this.gameObject.transform.position))
            {
             
                Debug.Log("Added");
                Weapons.Add(other.gameObject);
            }
           
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Weapon")
        {
            
                Debug.Log("Removed");
                Weapons.Remove(other.gameObject);
            

        }
    }



}
