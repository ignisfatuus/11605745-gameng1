﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class PlayGame : MonoBehaviour {

    public Health Player;
    public Text GameOver;
    public Button GOButton;
    public Button TitleScrene;
    public PlayerController PlayerControllerr;
	// Use this for initialization
	void Start ()
    {

        Player = GameObject.Find("Rowel").GetComponent<Health>();
	}
	
	// Update is called once per frame
	void Update () {
        SetGameOver();
	}

    public void LoadInGame()
    {
        SceneManager.LoadScene(1);
    }

    public void SetGameOver()
    {
        if (Player.CurrentHp==0)
        {
            GameOver.gameObject.SetActive(true);
            GOButton.gameObject.SetActive(true);
            TitleScrene.gameObject.SetActive(true);
        }
    }

    public void LoadInGameeee()
    {
        SceneManager.LoadScene(0);
    }




}
