﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ArrowCount : MonoBehaviour {

    public int Arrows;
    public Inventory InventoryManager;
    public Text UiText;
	// Use this for initialization
	void Start () {
        UiText = gameObject.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        Arrows  = InventoryManager.Arrows.Count;
        UiText.text = Arrows.ToString();
	}
}
