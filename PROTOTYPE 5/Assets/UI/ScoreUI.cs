﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScoreUI : MonoBehaviour {

    // Use this for initialization
    public Text ScoreText;
    public Score PlayerScore;
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        SetText();
	}

    void SetText()
    {
        ScoreText.text = ("Score: " + PlayerScore.ScoreAmount);
    }
}
