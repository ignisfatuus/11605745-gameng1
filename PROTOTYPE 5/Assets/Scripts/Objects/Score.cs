﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour {

    public int ScoreAmount;
	// Use this for initialization

	void Start ()
    {
        ScoreAmount = 0;
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
    
    public void IncreaseScore(int increment)
    {
        ScoreAmount += increment;
    }

    public void DecreaseScore(int decrement)
    {
        ScoreAmount -= decrement;
    }


}
