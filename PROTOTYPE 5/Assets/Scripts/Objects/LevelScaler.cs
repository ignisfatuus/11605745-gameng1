﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelScaler : MonoBehaviour {

    public PlayerMovement PlayerMovementScript;
    public ObjectSpawner ObjectSpawnerScript;
    public Score PlayerScore;
	// Use this for initialization
	void Start () {
        StartCoroutine(ScaleLevel());
	}
	
	// Update is called once per frame
	void Update () {

       
	}

    IEnumerator ScaleLevel()
    {
        while (true)
        {

            ObjectSpawnerScript.SpawnInterval -= 0.25f;
            PlayerMovementScript.VerticalSpeed -= 0.01f;
            yield return new WaitForSeconds(15);
        }
    }
}
