﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMovement : MonoBehaviour {

    // Use this for initialization

    public float MaxMoveSpeed;
    public float MaxSize;
    public float Direction;

    [SerializeField]private float MoveSpeed;

    private void OnEnable()
    {
      
    }

    private void Start()
    {
        //set Size
        MaxSize = Random.Range(1, MaxSize);
        gameObject.transform.localScale *= MaxSize;

        //set Speed
        MoveSpeed = Random.Range(0.035f, 0.05f);
        MoveSpeed *= Direction;
    }

    // Update is called once per frame
    void Update ()
    {
       
        Movement();
	}

    void Movement()
    {
        
        gameObject.transform.position += new Vector3(MoveSpeed, 0);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            PlayerMovement PlayerMovementScript = collision.GetComponent<PlayerMovement>();
            PlayerMovementScript.isAlive = false;
            collision.gameObject.SetActive(false);
        }
    }
    //void RandomizeDirection()
    //{
    //    int randomNumber = Random.Range(0, 100);
    //    if (randomNumber<=50)
    //    {
    //        direction = 1;
    //    }
    //    else if (randomNumber>50)
    //    {
    //        direction = -1;
    //    }
    //}
}
