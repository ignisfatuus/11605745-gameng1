﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour {

    private float ScoreAmount;
	// Use this for initialization
    public float GetScoreAmount
    {
       get { return ScoreAmount; }
    }
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void addScore(int amount)
    {
        ScoreAmount += amount;
    }

    public  void deductScore(int amount)
    {
        ScoreAmount += amount;
    }
}
