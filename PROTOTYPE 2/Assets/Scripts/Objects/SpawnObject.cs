﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObject : MonoBehaviour 
{

    public GameObject ObjectToSpawn; 

    public float SpawnInterval = 1.0f;
    public int SpawnCount = 1;
    public int MaxSpawnCount;

    public Vector2 MinPosition, MaxPosition;
    private List<GameObject> spawned = new List<GameObject>();
    private List<GameObject> pooledObjects = new List<GameObject>();

    void Start()
    {
        StartCoroutine(SpawnTask());
    }

    private void RandomizeSpawnInterval(float MinValue,float MaxValue)
    {
            
            
            SpawnInterval = Random.Range(MinValue, MaxValue);
        
    }
    IEnumerator SpawnTask()
    {
        for (int x = 0; x < MaxSpawnCount; x++)
        {
            {
                // Wait for spawn interval
                RandomizeSpawnInterval(2,4);
             

                for (int i = 0; i < SpawnCount; i++)
                {

                    // Spawn the object. If we have an object in the pool, use that instead. Else, instantiate.
                    GameObject obj;
                    if (pooledObjects.Count > 0)
                    {
                        obj = pooledObjects[0];
                        pooledObjects.RemoveAt(0);
                        obj.SetActive(true);
                    }
                    else
                    {
                        obj = Instantiate(ObjectToSpawn);
                    }

                    // Randomize position
                    obj.transform.position = new Vector3(Random.Range(MinPosition.x, MaxPosition.x), Random.Range(MinPosition.y, MaxPosition.y), 0);

                    spawned.Add(obj);


                }

                yield return new WaitForSeconds(SpawnInterval);

            }
        }
    }


}
