﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartBehavior : MonoBehaviour {

    // Use this for initialization
    public PlayerMovement PlayerMovementScript;
    public ScoreManager ScoreManagerScript;
    public HeartBehavior HeartPair;
    public CoinSpawner CoinSpawnerScript;
    public float Directon;
    private bool canSpawn;
    private bool  canScore;
	void Start ()
    {
        canScore = true;
        canSpawn = true;
	}
	
	// Update is called once per frame
	void Update ()

    {
		
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            PlayerMovementScript.InverseDirection();
          
            if(canScore)
            {
                ScoreManagerScript.addScore(1); 
            }
            canScore = false;
            HeartPair.canScore = true;

         

            if (canSpawn)
            {
                CoinSpawnerScript.Coin.transform.position = new Vector3(0, HeartPair.transform.position.y + (0.5f * Directon));
                CoinSpawnerScript.SpawnObject();
            }
            canSpawn = false;
            HeartPair.canSpawn = true;
            //CoinSpawnerScript.Object.transform.position = new Vector3(HeartPair.transform.position.x,HeartPair.transform.position.y)
        }
    }

 
}
