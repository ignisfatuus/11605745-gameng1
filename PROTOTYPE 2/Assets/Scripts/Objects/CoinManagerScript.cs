﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinManagerScript : MonoBehaviour {

    // Use this for initialization
    private float CoinAmount;

    public float GetCoinAmount
    {
        get { return CoinAmount; }
    }
    void Start ()
    {
		
	}

    // Update is called once per frame
    void Update()
    {

    }

    public void addCoinAmount(int amount)
    {
        CoinAmount += amount;
    }

    public void deductScore(int amount)
    {
        CoinAmount += amount;
    }
}
