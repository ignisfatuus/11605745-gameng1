﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    // Use this for initialization
    public float VerticalSpeed;
    public float YDirection;
    public bool isAlive;
    [SerializeField]private float ZRotation;

	void Start ()
    {
        isAlive = true;
        YDirection = 1;
        VerticalSpeed *= YDirection;
        ZRotation = 0;
    }
	
	// Update is called once per frame
	void Update ()
    {
        Movement();
	}

    private void Movement()
    {
       if (Input.GetMouseButtonDown(0))
        {
            InverseDirection();

        }

        gameObject.transform.position += new Vector3(0, VerticalSpeed);
    }

    private void Rotate()
    {
        ZRotation += 180;
       gameObject.transform.localRotation = Quaternion.Euler(0, 0,   ZRotation);
    }

    public  void InverseDirection()
    {
        YDirection = -1;
        VerticalSpeed *= YDirection;
        Rotate();
    }

}
