﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {

    public CoinManagerScript CoinManagerScript;
    public int Amount;
	// Use this for initialization
	void Start ()
    {
        CoinManagerScript = GameObject.Find("CoinManager").GetComponent<CoinManagerScript>(); 
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            CoinManagerScript.addCoinAmount(Amount);
            Destroy(gameObject);
        }
    }
}
