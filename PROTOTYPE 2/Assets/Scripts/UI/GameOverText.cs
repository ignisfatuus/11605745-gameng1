﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameOverText : MonoBehaviour {

    public PlayerMovement PlayerMovementScript;
    public Text GameOverUI;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(!PlayerMovementScript.isAlive)
        {
            GameOverUI.gameObject.SetActive(true);
        }
	}
}
