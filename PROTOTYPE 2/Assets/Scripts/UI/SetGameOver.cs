﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement; 
public class SetGameOver : MonoBehaviour {

    // Use this for initialization
    public PlayerMovement PlayerMoveScript;
    public GameObject GameOverUI;
    public Text CurrentScore, HighScore, CurrentScoreNumber, HighScoreNumber,TotalCoins;
    public ScoreManager ScoreManagerScript;
    public CoinManagerScript CoinManager;
    private float CoinAmount;
    public SpawnObject LeftSpawner, RightSpawner;

	void Start ()
    {
        CoinAmount = PlayerPrefs.GetFloat("Coins");

    }
	
	// Update is called once per frame
	void Update ()
    {
        ActivateGameOverUI();
        SetTexts();
	}

    void ActivateGameOverUI()
    {
        if (!PlayerMoveScript.isAlive)
        {
            Debug.Log("hatdog");
            GameOverUI.SetActive(true);
            
            LeftSpawner.gameObject.SetActive(false);
            RightSpawner.gameObject.SetActive(false);
        }
        else
   
        
            GameOverUI.SetActive(false);
        LeftSpawner.gameObject.SetActive(true);
        RightSpawner.gameObject.SetActive(true);
    }

    void SetTexts()
    {
        CurrentScore.text = ScoreManagerScript.GetScoreAmount.ToString();
        CoinAmount += CoinManager.GetCoinAmount;
        PlayerPrefs.SetFloat("Coins", CoinManager.GetCoinAmount);
        TotalCoins.text = ("Total:" + PlayerPrefs.GetFloat("Coins"));
        if (ScoreManagerScript.GetScoreAmount>PlayerPrefs.GetFloat("HighScore"))
        {
            PlayerPrefs.SetFloat("HighScore", ScoreManagerScript.GetScoreAmount);
        }
        HighScore.text = PlayerPrefs.GetFloat("HighScore").ToString();


    }

    public void RetryButton()
    {
        Scene loadedLevel = SceneManager.GetActiveScene();
        SceneManager.LoadScene(loadedLevel.buildIndex);
    }
}
