﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SetCoinText : MonoBehaviour {

    public Text ObjectText;
    public CoinManagerScript CoinManagerScript;
    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        ObjectText.text = "Coins:" + CoinManagerScript.GetCoinAmount;
    }
}
